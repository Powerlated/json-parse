const inquirer = require('inquirer');
const parseArgs = require('minimist');

const argv = parseArgs(process.argv);

const fileType = require('file-type');

const JSONStream = require('JSONStream');
const es = require('event-stream');

let c = argv.c;
let s = argv.s;
let j = argv.j;
let v = argv.v;
let l = argv.l;
let f = argv.f;
let fulldata = argv.fulldata;

if (!argv.h) {
	console.log('Tip: Pass -h for help');
} else {
	console.log('-c: to parse standard input. Using -c also means -s is required');
	console.log('-s: one or more to specify subreddits from the command line');
	console.log('-j: to specify a JSON file from the command line');
	console.log('-v: to make the program print some extra info');
	console.log('-l: to save only the comment body');
	console.log('-f: to force overwriting existing files');
	console.log('--fulldata: save all attributes instead of just basic data');
	process.exit(0);
}

if (f) {
	console.log('Overwriting existing files');
}

if (c && !s) {
	console.log('Error: -s is required when using -c');
	process.exit(1);
}
if (c && j) {
	console.log('Error: Cannot have -c and -j at the same time');
	process.exit(1);
}

if (fulldata && l) {
	console.log('Error: Cannot have -l and --fulldata and the same time');
	process.exit(1);
}

Array.prototype.isArray = true;

const fs = require('graceful-fs');

if (!fs.existsSync('./output')) {
	fs.mkdirSync('./output');
}

let sourceFilename;
let subredditName;

function validateJSONInputStream(inputStream) {
	inputStream
		.pipe(JSONStream.parse())
		.on('error', function(error) {
			return false;
		})
		.pipe(
			es.mapSync(function(data) {
				return true;
			})
		);
}

const questions = [
	{
		name: 'sourceFilename',
		message: 'JSON Source Filename (Uncompressed)',
		required: true,
		filter: (value) => {
			return new Promise((resolve, reject) => {
				if (value.length == 0) {
					reject('Cannot be empty');
				}
				if (!fs.existsSync(`./${value}`)) {
					reject(`'${value}' does not exist`);
				}
				if (fs.lstatSync(value).isDirectory()) {
					reject('Cannot be directory');
				}

				let valid = true;
				const readChunk = require('read-chunk');

				const buffer = readChunk.sync(`./${value}`, 0, 4100);
				let type = fileType(buffer);

				let inputStream = fs.createReadStream(`./${value}`);

				let jsonError = validateJSONInputStream(inputStream);

				inputStream.close();

				inputStream.on('close', () => {
					let errorMessage = '';

					if (type != undefined && type.ext != 'json') {
						errorMessage = 'File is not JSON';
					}
					if (jsonError) {
						errorMessage = `The file ${value} is invalid JSON`;
					}

					if (errorMessage.length == 0) {
						resolve(value);
						sourceFilename = value;
					} else {
						reject(errorMessage);
					}
				});
			});
		},
		when: (!c && !j) || (j != undefined && j.length != 0 && j == true),
		default: j
	},
	{
		name: 'subredditName',
		message: 'Subreddit Name (all for everything)',
		required: true,
		filter: (value) => {
			return new Promise((resolve, reject) => {
				let valid = true;
				if (value.length == 0) {
					valid = false;
					errorMessage = 'Cannot be empty';
				} else if (!/^\w+$/.test(value) && value == 'all') {
					valid = false;
					errorMessage = 'Needs to be alphanumerical';
				}

				subredditName = value;

				if (valid) {
					resolve(value);
				} else {
					reject(errorMessage);
				}
			});
		},
		when: !s,
		default: s
	},
	{
		name: 'areYouSure',
		type: 'confirm',
		message: () => {
			return `Are you sure you want to overwrite ${subredditName ? subredditName : s}.json`;
		},
		when: fs.existsSync(`./${sourceFilename}.json`) && !f && f != undefined && f.isArray,
		default: true
	}
];

Array.prototype.times = function(f) {
	for (v of this) for (var _ of Array(v)) f();
};

inquirer
	.prompt(questions)
	.then((answers) => {
		if (answers.areYouSure != undefined && !answers.areYouSure) process.exit(0);

		let sourceFilename = answers.sourceFilename;
		let subredditName = answers.subredditName;

		let inFile;
		if (c) {
			inFile = process.stdin;
			console.log('Reading from standard input');
		} else {
			if (j) {
				inFile = fs.createReadStream(`./${j}`);
			} else {
				inFile = fs.createReadStream(`./${sourceFilename}`);
			}
		}

		let type = fileType(inFile);

		let uniqueSubs =
			typeof s == 'string'
				? [ s.toLowerCase() ]
				: Array.from(new Set(s.toLocaleString().toLowerCase().split(',')));

		if (s && s.isArray && s.length > 1 && !f) {
			console.log('You can only specify multiple subs when forcing overwrite');
			process.exit(1);
		}

		if (typeof s == 'string') subredditName = s;

		let subData = {};
		let subDataCounter = {};

		let vTimer;
		let vTimerElapsed = new Date().getTime();
		let bytesPiped = 1;
		let totalBytesPiped = 0;
		let commentsDone = 1;
		if (v) {
			vTimer = setInterval(() => {
				let timeElapsed = new Date().getTime() - vTimerElapsed;
				let secondsElapsed = timeElapsed / 1000;
				vTimerElapsed = new Date().getTime();
				Object.entries(subDataCounter).forEach(([ sub, count ]) => {
					console.log(
						`[${process.uptime().toFixed(3)} s:${secondsElapsed.toFixed(3)} ${(bytesPiped /
							1048576 /
							secondsElapsed).toFixed(1)}MB/s ${(totalBytesPiped / 1048576).toFixed(1)}MB ${Math.floor(
							commentsDone / secondsElapsed
						)}cps] ${sub}, ${count} comment${count > 1 ? 's' : ''}`
					);
				});
				bytesPiped = 0;
				commentsDone = 0;
			}, 1000);
		} else {
			vTimer = setInterval(() => {
				let timeElapsed = new Date().getTime() - vTimerElapsed;
				let secondsElapsed = timeElapsed / 1000;
				vTimerElapsed = new Date().getTime();
				console.log(
					`[${process.uptime().toFixed(3)} s:${secondsElapsed.toFixed(3)} ${(bytesPiped /
						1048576 /
						secondsElapsed).toFixed(1)}MB/s ${(totalBytesPiped / 1048576).toFixed(1)}MB ${Math.floor(
						commentsDone / secondsElapsed
					)}cps]`
				);
				bytesPiped = 0;
				commentsDone = 0;
			}, 1000);
		}

		let commentsParsed = 0;

    let re = new RegExp(subredditName, 'i');
		function regexArrayTest() {
			let matched = false;
			uniqueSubs.forEach((v, i) => {
        if (re.test(v)) {
          matched = true;
        }
      });
      return matched;
		}

		inFile
			.pipe(JSONStream.parse())
			.on('error', function(error) {
				console.log(error);
				process.exit(1);
			})
			.pipe(
				es.mapSync(function(data) {
					
					if (
						(subredditName && re.test(data.subreddit)) ||
						regexArrayTest() ||
						subredditName == 'all'
					) {
						commentsDone++;
						if (!subData[data.subreddit]) {
							const stream = require('stream');
							subData[data.subreddit] = new stream.Duplex({ objectMode: true });
							subData[data.subreddit]._read = function() {};
							if (fs.existsSync(`./output/${data.subreddit}.json`)) {
								fs.unlinkSync(`./output/${data.subreddit}.json`);
							}
							let outputFile = fs.createWriteStream(`./output/${data.subreddit}.json`);
							subData[data.subreddit].pipe(JSONStream.stringify(false)).pipe(outputFile);
						}
						if (v) {
							if (isNaN(subDataCounter[data.subreddit])) {
								subDataCounter[data.subreddit] = 0;
							}
							subDataCounter[data.subreddit]++;
						}
						if (fulldata) {
							subData[data.subreddit].push(data);
						} else if (!l) {
							subData[data.subreddit].push({
								body: data.body,
								score: data.score,
								link_id: data.link_id,
								subreddit: data.subreddit,
								created_utc: data.created_utc
							});
						} else {
							subData[data.subreddit].push(data.body);
						}
					}
				})
			);

		inFile.on('end', () => {
			if (vTimer) {
				clearTimeout(vTimer);
			}
			console.log('Done.');
			process.exit(0);
		});

		inFile.on('data', (buffer) => {
			bytesPiped += buffer.toString().length;
			totalBytesPiped += buffer.toString().length;
		});
	})
	.catch((e) => {
		console.log('Error:');
		console.log(e);
	});
